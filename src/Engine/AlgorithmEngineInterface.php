<?php

declare(strict_types=1);

namespace SignKit\Engine;

interface AlgorithmEngineInterface
{
    /**
     * 设置秘钥
     * @param string $secret
     * @return bool
     */
    public function setSecret(string $secret): bool;

    /**
     * 生成签名
     * @param array $params
     * @return string
     */
    public function generate(array $params): string;

    /**
     * @return array
     */
    public function getDebugInfo(): array;
}