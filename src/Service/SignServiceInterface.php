<?php

declare(strict_types=1);

namespace SignKit\Service;

interface SignServiceInterface
{
    /**
     * 设置排除键值
     * @param array $excludeKeys
     * @return bool
     */
    public function setExcludeKeys(array $excludeKeys): bool;

    /**
     * 生成签名
     * @param array $params
     * @param string $secret
     * @return string
     */
    public function generate(array $params, string $secret): string;

    /**
     * 验证签名
     * @param array $params
     * @param string $secret
     * @return bool
     */
    public function verify(array $params, string $secret): bool;

    /**
     * 补全参数
     * @param array $params
     * @param string $secret
     * @return array
     */
    public function attachParams(array $params, string $secret): array;

    /**
     * 获取debug信息
     * @return array
     */
    public function getDebugInfo(): array;
}