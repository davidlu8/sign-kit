<?php

declare(strict_types=1);

namespace SignKit\Service;

use Contract\Exceptions\ValidationException;
use SignKit\Engine\AlgorithmEngineInterface;

class CommonSignService implements SignServiceInterface
{
    const KEY_SIGN = 'sign';
    const KEY_TIMESTAMP = 'timestamp';
    const KEY_NONCE_STR = 'nonce_str';

    /** @var array $excludeKeys */
    private array $excludeKeys = [];

    private AlgorithmEngineInterface $algorithmEngine;

    public function __construct(AlgorithmEngineInterface $algorithmEngine)
    {
        $this->algorithmEngine = $algorithmEngine;
    }

    /**
     * @param array $excludeKeys
     * @return bool
     */
    public function setExcludeKeys(array $excludeKeys): bool
    {
        $this->excludeKeys = $excludeKeys;
        return true;
    }

    /**
     * 获取最终排除的参数键值
     * @return array
     */
    private function getFinalExcludeKeys(): array
    {
        if (!is_array($this->excludeKeys)) {
            $this->excludeKeys = [];
        }
        return array_merge($this->excludeKeys, [self::KEY_SIGN]);
    }

    /**
     * @param array $params
     * @return array
     */
    private function filterExcludeParams(array $params): array
    {
        $excludeKeys = $this->getFinalExcludeKeys();
        $newParams = [];
        foreach ($params as $key => $value) {
            if (!in_array($key, $excludeKeys)) {
                $newParams[$key] = $value;
            }
        }
        return $newParams;
    }

    /**
     * @param array $params
     * @param string $secret
     * @return string
     * @throws ValidationException
     */
    public function generate(array $params, string $secret): string
    {
        $this->validate($params, $secret);
        $params = $this->filterExcludeParams($params);
        $this->algorithmEngine->setSecret($secret);
        return $this->algorithmEngine->generate($params);
    }

    /**
     * @param array $params
     * @param string $secret
     * @return bool
     * @throws ValidationException
     */
    public function verify(array $params, string $secret): bool
    {
        $this->validate($params, $secret);
        if (!isset($params[self::KEY_TIMESTAMP]) || empty($params[self::KEY_TIMESTAMP])) {
            throw new ValidationException('时间戳不能为空');
        }
        if (!isset($params[self::KEY_NONCE_STR]) || empty($params[self::KEY_NONCE_STR])) {
            throw new ValidationException('一次性字符串不能为空');
        }
        if (!isset($params[self::KEY_SIGN]) || empty($params[self::KEY_SIGN])) {
            throw new ValidationException('签名不能为空');
        }

        $actualSign = (string)$params[self::KEY_SIGN];
        $params = $this->filterExcludeParams($params);
        $this->algorithmEngine->setSecret($secret);
        $expectSign = $this->algorithmEngine->generate($params);
        if ($expectSign === $actualSign) {
            return true;
        }
        return false;
    }

    /**
     * 补全参数
     * @param array $params
     * @param string $secret
     * @return array
     * @throws ValidationException
     */
    public function attachParams(array $params, string $secret): array
    {
        $this->validate($params, $secret);
        $params = $this->filterExcludeParams($params);
        $params = $this->attachFixedParams($params);
        $this->algorithmEngine->setSecret($secret);
        $expectedSign = $this->algorithmEngine->generate($params);
        $params[self::KEY_SIGN] = $expectedSign;
        return $params;
    }

    /**
     * 新增固定参数
     * @param array $params
     * @return array
     */
    private function attachFixedParams(array $params): array
    {
        if (!isset($params[self::KEY_TIMESTAMP]) || empty($params[self::KEY_TIMESTAMP])) {
            $params[self::KEY_TIMESTAMP] = time();
        }
        if (!isset($params[self::KEY_NONCE_STR]) || empty($params[self::KEY_NONCE_STR])) {
            $params[self::KEY_NONCE_STR] = $this->getNonceStr();
        }
        return $params;
    }

    /**
     * @param int $length
     * @return string
     */
    private function getNonceStr(int $length = 10): string
    {
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $charLength = strlen($chars);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, $charLength - 1), 1);
        }
        return $str;
    }

    /**
     * @param array $params
     * @param string $secret
     * @return void
     * @throws ValidationException
     */
    private function validate(array $params, string $secret): void
    {
        if (empty($params)) {
            throw new ValidationException('参数不能为空');
        }
        if (empty($secret)) {
            throw new ValidationException('秘钥不能为空');
        }
    }

    /**
     * @return array
     */
    public function getDebugInfo(): array
    {
        return $this->algorithmEngine->getDebugInfo();
    }
}