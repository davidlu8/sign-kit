<?php

declare(strict_types=1);

namespace SignKit\Tests\Service;

use Contract\Exceptions\ValidationException;
use PHPUnit\Framework\MockObject\MockObject;
use SignKit\Engine\FullParameterAlgorithmEngine;
use SignKit\Service\CommonSignService;
use SignKit\Tests\TestCase;

class CommonSignServiceTest extends TestCase
{
    /**
     * @return array
     */
    public function dpGenerateSceneExcludeKeysAssertTrue(): array
    {
        return [
            [
                [
                    'a' => '1',
                    'b' => '2',
                    'c' => '3',
                    'sign' => '4'
                ],
                [],
                [
                    'a' => '1',
                    'b' => '2',
                    'c' => '3',
                ]
            ],
            [
                [
                    'a' => '1',
                    'b' => '2',
                    'c' => '3',
                    'sign' => '4'
                ],
                ['a', 'b'],
                [
                    'c' => '3',
                ]
            ],
        ];
    }

    /**
     * @dataProvider dpGenerateSceneExcludeKeysAssertTrue
     * @param $params
     * @param $excludeKeys
     * @param $exceptedParams
     * @throws ValidationException
     */
    public function testGenerateSceneExcludeKeysAssertTrue($params, $excludeKeys, $exceptedParams)
    {
        /** @var MockObject|FullParameterAlgorithmEngine $algorithmEngineMock */
        $algorithmEngineMock = $this->getMockBuilder(FullParameterAlgorithmEngine::class)
            ->disableOriginalConstructor()
            ->setMethods(['generate'])
            ->getMock();
        $algorithmEngineMock->expects($this->once())
            ->method('generate')
            ->with($this->equalTo($exceptedParams));

        $signManager = new CommonSignService($algorithmEngineMock);
        $signManager->setExcludeKeys($excludeKeys);
        $signManager->generate($params, 'xxx');
    }


    /**
     * @throws ValidationException
     */
    public function testAttachSceneAttachAssertTrue()
    {
        /** @var FullParameterAlgorithmEngine|MockObject $algorithmEngineStub */
        $algorithmEngineStub = $this->getMockBuilder(FullParameterAlgorithmEngine::class)
            ->disableOriginalConstructor()
            ->setMethods(['generate'])
            ->getMock();
        $algorithmEngineStub->method('generate')
            ->willReturn('yyy');

        $signManager = new CommonSignService($algorithmEngineStub);
        $params = $signManager->attachParams([
            'a' => 1
        ], 'xxx');
        $this->assertTrue(isset($params['sign']));
        $this->assertTrue(isset($params['timestamp']));
        $this->assertTrue(isset($params['nonce_str']));
        $this->assertTrue($params['sign'] == 'yyy');
    }
}