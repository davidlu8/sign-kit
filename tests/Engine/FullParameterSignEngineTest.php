<?php

declare(strict_types=1);

namespace SignKit\Tests\Engine;

use SignKit\Engine\FullParameterAlgorithmEngine;
use SignKit\Tests\TestCase;
use stdClass;

class FullParameterSignEngineTest extends TestCase
{
    private FullParameterAlgorithmEngine $fullParameterSignEngine;

    public function setUp()
    {
        $this->fullParameterSignEngine = new FullParameterAlgorithmEngine();
    }

    public function dpParseParams(): array
    {
        $emptyStd = new stdClass();
        $std = new stdClass();
        $std->d1 = 1;
        $std->d2 = [
            'v2', 'f' => [
                'f1'
            ]
        ];
        return [
            [
                [
                    'a' => '',
                    'b' => 1,
                    'c' => null,
                    'd' => true,
                    'e' => false,
                    'f' => [],
                    'g' => $emptyStd,
                ],
                [
                    'b' => 1,
                    'd' => 1,
                    'e' => 0,
                ]
            ],
            [
                [
                    'a' => 1,
                    'b' => 'ccc',
                    'c' => [
                        'c1' => 1,
                        'c2' => 2
                    ],
                    'd' => $std
                ],
                [
                    'a' => 1,
                    'b' => 'ccc',
                    'c.c1' => 1,
                    'c.c2' => 2,
                    'd.d1' => 1,
                    'd.d2.0' => 'v2',
                    'd.d2.f.0' => 'f1',
                ]
            ]
        ];
    }

    /**
     * @dataProvider dpParseParams
     * @param $params
     * @param $parseParams
     */
    public function testParseParams($params, $parseParams)
    {
        $this->assertEquals($parseParams, $this->fullParameterSignEngine->parseParams($params));
    }
}